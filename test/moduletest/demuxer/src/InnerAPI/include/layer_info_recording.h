/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LAYER_INFO_RECORDING_H
#define LAYER_INFO_RECORDING_H

#include <iostream>

auto GopInfoRecording = R"([
    {"gopId": 0, "gopSize": 100, "startFrameId": 0},
    {"gopId": 1, "gopSize": 100, "startFrameId": 100},
    {"gopId": 2, "gopSize": 65, "startFrameId": 200}
])"_json;

auto FrameLayerInfoRecording = R"([
    {"frameId": 0, "dts": 0, "layer": 1, "discardable": false},
    {"frameId": 1, "dts": 15066, "layer": 0, "discardable": false},
    {"frameId": 2, "dts": 31422, "layer": 0, "discardable": false},
    {"frameId": 3, "dts": 54844, "layer": 0, "discardable": false},
    {"frameId": 4, "dts": 62711, "layer": 0, "discardable": false},
    {"frameId": 5, "dts": 78244, "layer": 0, "discardable": false},
    {"frameId": 6, "dts": 95966, "layer": 0, "discardable": false},
    {"frameId": 7, "dts": 112177, "layer": 0, "discardable": false},
    {"frameId": 8, "dts": 128400, "layer": 0, "discardable": false},
    {"frameId": 9, "dts": 145177, "layer": 0, "discardable": false},
    {"frameId": 10, "dts": 163055, "layer": 0, "discardable": false},
    {"frameId": 11, "dts": 179111, "layer": 0, "discardable": false},
    {"frameId": 12, "dts": 199666, "layer": 0, "discardable": false},
    {"frameId": 13, "dts": 213411, "layer": 0, "discardable": false},
    {"frameId": 14, "dts": 230488, "layer": 0, "discardable": false},
    {"frameId": 15, "dts": 246922, "layer": 0, "discardable": false},
    {"frameId": 16, "dts": 264455, "layer": 0, "discardable": false},
    {"frameId": 17, "dts": 283244, "layer": 0, "discardable": false},
    {"frameId": 18, "dts": 303022, "layer": 0, "discardable": false},
    {"frameId": 19, "dts": 317888, "layer": 0, "discardable": false},
    {"frameId": 20, "dts": 335000, "layer": 0, "discardable": false},
    {"frameId": 21, "dts": 349522, "layer": 0, "discardable": false},
    {"frameId": 22, "dts": 367744, "layer": 0, "discardable": false},
    {"frameId": 23, "dts": 383477, "layer": 0, "discardable": false},
    {"frameId": 24, "dts": 401322, "layer": 0, "discardable": false},
    {"frameId": 25, "dts": 417200, "layer": 0, "discardable": false},
    {"frameId": 26, "dts": 433044, "layer": 0, "discardable": false},
    {"frameId": 27, "dts": 449566, "layer": 0, "discardable": false},
    {"frameId": 28, "dts": 468522, "layer": 0, "discardable": false},
    {"frameId": 29, "dts": 483077, "layer": 0, "discardable": false},
    {"frameId": 30, "dts": 502155, "layer": 0, "discardable": false},
    {"frameId": 31, "dts": 516688, "layer": 0, "discardable": false},
    {"frameId": 32, "dts": 533811, "layer": 0, "discardable": false},
    {"frameId": 33, "dts": 549011, "layer": 0, "discardable": false},
    {"frameId": 34, "dts": 569000, "layer": 0, "discardable": false},
    {"frameId": 35, "dts": 584533, "layer": 0, "discardable": false},
    {"frameId": 36, "dts": 599811, "layer": 0, "discardable": false},
    {"frameId": 37, "dts": 617366, "layer": 0, "discardable": false},
    {"frameId": 38, "dts": 632244, "layer": 0, "discardable": false},
    {"frameId": 39, "dts": 650777, "layer": 0, "discardable": false},
    {"frameId": 40, "dts": 665755, "layer": 0, "discardable": false},
    {"frameId": 41, "dts": 683722, "layer": 0, "discardable": false},
    {"frameId": 42, "dts": 702488, "layer": 0, "discardable": false},
    {"frameId": 43, "dts": 716588, "layer": 0, "discardable": false},
    {"frameId": 44, "dts": 732200, "layer": 0, "discardable": false},
    {"frameId": 45, "dts": 751644, "layer": 0, "discardable": false},
    {"frameId": 46, "dts": 765688, "layer": 0, "discardable": false},
    {"frameId": 47, "dts": 784244, "layer": 0, "discardable": false},
    {"frameId": 48, "dts": 798688, "layer": 0, "discardable": false},
    {"frameId": 49, "dts": 815433, "layer": 0, "discardable": false},
    {"frameId": 50, "dts": 832577, "layer": 0, "discardable": false},
    {"frameId": 51, "dts": 842322, "layer": 0, "discardable": false},
    {"frameId": 52, "dts": 932344, "layer": 0, "discardable": false},
    {"frameId": 53, "dts": 1127000, "layer": 0, "discardable": false},
    {"frameId": 54, "dts": 1940200, "layer": 0, "discardable": false},
    {"frameId": 55, "dts": 2922488, "layer": 0, "discardable": false},
    {"frameId": 56, "dts": 3247233, "layer": 0, "discardable": false},
    {"frameId": 57, "dts": 3253466, "layer": 0, "discardable": false},
    {"frameId": 58, "dts": 3274033, "layer": 0, "discardable": false},
    {"frameId": 59, "dts": 3296188, "layer": 0, "discardable": false},
    {"frameId": 60, "dts": 3302622, "layer": 0, "discardable": false},
    {"frameId": 61, "dts": 3324244, "layer": 0, "discardable": false},
    {"frameId": 62, "dts": 3341744, "layer": 0, "discardable": false},
    {"frameId": 63, "dts": 3357733, "layer": 0, "discardable": false},
    {"frameId": 64, "dts": 3374511, "layer": 0, "discardable": false},
    {"frameId": 65, "dts": 3393444, "layer": 0, "discardable": false},
    {"frameId": 66, "dts": 3409055, "layer": 0, "discardable": false},
    {"frameId": 67, "dts": 3427811, "layer": 0, "discardable": false},
    {"frameId": 68, "dts": 3442977, "layer": 0, "discardable": false},
    {"frameId": 69, "dts": 3459200, "layer": 0, "discardable": false},
    {"frameId": 70, "dts": 3476544, "layer": 0, "discardable": false},
    {"frameId": 71, "dts": 3493511, "layer": 0, "discardable": false},
    {"frameId": 72, "dts": 3510066, "layer": 0, "discardable": false},
    {"frameId": 73, "dts": 3527155, "layer": 0, "discardable": false},
    {"frameId": 74, "dts": 3543288, "layer": 0, "discardable": false},
    {"frameId": 75, "dts": 3559811, "layer": 0, "discardable": false},
    {"frameId": 76, "dts": 3575522, "layer": 0, "discardable": false},
    {"frameId": 77, "dts": 3593722, "layer": 0, "discardable": false},
    {"frameId": 78, "dts": 3609322, "layer": 0, "discardable": false},
    {"frameId": 79, "dts": 3629511, "layer": 0, "discardable": false},
    {"frameId": 80, "dts": 3643044, "layer": 0, "discardable": false},
    {"frameId": 81, "dts": 3657777, "layer": 0, "discardable": false},
    {"frameId": 82, "dts": 3673255, "layer": 0, "discardable": false},
    {"frameId": 83, "dts": 3690988, "layer": 0, "discardable": false},
    {"frameId": 84, "dts": 3707666, "layer": 0, "discardable": false},
    {"frameId": 85, "dts": 3724622, "layer": 0, "discardable": false},
    {"frameId": 86, "dts": 3742155, "layer": 0, "discardable": false},
    {"frameId": 87, "dts": 3760322, "layer": 0, "discardable": false},
    {"frameId": 88, "dts": 3774077, "layer": 0, "discardable": false},
    {"frameId": 89, "dts": 3793422, "layer": 0, "discardable": false},
    {"frameId": 90, "dts": 3809066, "layer": 0, "discardable": false},
    {"frameId": 91, "dts": 3827333, "layer": 0, "discardable": false},
    {"frameId": 92, "dts": 3844111, "layer": 0, "discardable": false},
    {"frameId": 93, "dts": 3861000, "layer": 0, "discardable": false},
    {"frameId": 94, "dts": 3877633, "layer": 0, "discardable": false},
    {"frameId": 95, "dts": 3894022, "layer": 0, "discardable": false},
    {"frameId": 96, "dts": 3907077, "layer": 0, "discardable": false},
    {"frameId": 97, "dts": 3925744, "layer": 0, "discardable": false},
    {"frameId": 98, "dts": 3944322, "layer": 0, "discardable": false},
    {"frameId": 99, "dts": 3957011, "layer": 0, "discardable": false},
    {"frameId": 100, "dts": 3973777, "layer": 1, "discardable": false},
    {"frameId": 101, "dts": 3989966, "layer": 0, "discardable": false},
    {"frameId": 102, "dts": 4007888, "layer": 0, "discardable": false},
    {"frameId": 103, "dts": 4022988, "layer": 0, "discardable": false},
    {"frameId": 104, "dts": 4040433, "layer": 0, "discardable": false},
    {"frameId": 105, "dts": 4058866, "layer": 0, "discardable": false},
    {"frameId": 106, "dts": 4074677, "layer": 0, "discardable": false},
    {"frameId": 107, "dts": 4093000, "layer": 0, "discardable": false},
    {"frameId": 108, "dts": 4110011, "layer": 0, "discardable": false},
    {"frameId": 109, "dts": 4124288, "layer": 0, "discardable": false},
    {"frameId": 110, "dts": 4139288, "layer": 0, "discardable": false},
    {"frameId": 111, "dts": 4156811, "layer": 0, "discardable": false},
    {"frameId": 112, "dts": 4174444, "layer": 0, "discardable": false},
    {"frameId": 113, "dts": 4191522, "layer": 0, "discardable": false},
    {"frameId": 114, "dts": 4208311, "layer": 0, "discardable": false},
    {"frameId": 115, "dts": 4224033, "layer": 0, "discardable": false},
    {"frameId": 116, "dts": 4240288, "layer": 0, "discardable": false},
    {"frameId": 117, "dts": 4262333, "layer": 0, "discardable": false},
    {"frameId": 118, "dts": 4274400, "layer": 0, "discardable": false},
    {"frameId": 119, "dts": 4289188, "layer": 0, "discardable": false},
    {"frameId": 120, "dts": 4306266, "layer": 0, "discardable": false},
    {"frameId": 121, "dts": 4322288, "layer": 0, "discardable": false},
    {"frameId": 122, "dts": 4338855, "layer": 0, "discardable": false},
    {"frameId": 123, "dts": 4359255, "layer": 0, "discardable": false},
    {"frameId": 124, "dts": 4372900, "layer": 0, "discardable": false},
    {"frameId": 125, "dts": 4392800, "layer": 0, "discardable": false},
    {"frameId": 126, "dts": 4406666, "layer": 0, "discardable": false},
    {"frameId": 127, "dts": 4423244, "layer": 0, "discardable": false},
    {"frameId": 128, "dts": 4441744, "layer": 0, "discardable": false},
    {"frameId": 129, "dts": 4458922, "layer": 0, "discardable": false},
    {"frameId": 130, "dts": 4475500, "layer": 0, "discardable": false},
    {"frameId": 131, "dts": 4492344, "layer": 0, "discardable": false},
    {"frameId": 132, "dts": 4507655, "layer": 0, "discardable": false},
    {"frameId": 133, "dts": 4523844, "layer": 0, "discardable": false},
    {"frameId": 134, "dts": 4541788, "layer": 0, "discardable": false},
    {"frameId": 135, "dts": 4555566, "layer": 0, "discardable": false},
    {"frameId": 136, "dts": 4573833, "layer": 0, "discardable": false},
    {"frameId": 137, "dts": 4590155, "layer": 0, "discardable": false},
    {"frameId": 138, "dts": 4605144, "layer": 0, "discardable": false},
    {"frameId": 139, "dts": 4623222, "layer": 0, "discardable": false},
    {"frameId": 140, "dts": 4639033, "layer": 0, "discardable": false},
    {"frameId": 141, "dts": 4654422, "layer": 0, "discardable": false},
    {"frameId": 142, "dts": 4670666, "layer": 0, "discardable": false},
    {"frameId": 143, "dts": 4689633, "layer": 0, "discardable": false},
    {"frameId": 144, "dts": 4706211, "layer": 0, "discardable": false},
    {"frameId": 145, "dts": 4724177, "layer": 0, "discardable": false},
    {"frameId": 146, "dts": 4740255, "layer": 0, "discardable": false},
    {"frameId": 147, "dts": 4756033, "layer": 0, "discardable": false},
    {"frameId": 148, "dts": 4772511, "layer": 0, "discardable": false},
    {"frameId": 149, "dts": 4789077, "layer": 0, "discardable": false},
    {"frameId": 150, "dts": 4806055, "layer": 0, "discardable": false},
    {"frameId": 151, "dts": 4822511, "layer": 0, "discardable": false},
    {"frameId": 152, "dts": 4838644, "layer": 0, "discardable": false},
    {"frameId": 153, "dts": 4855744, "layer": 0, "discardable": false},
    {"frameId": 154, "dts": 4871177, "layer": 0, "discardable": false},
    {"frameId": 155, "dts": 4889566, "layer": 0, "discardable": false},
    {"frameId": 156, "dts": 4907933, "layer": 0, "discardable": false},
    {"frameId": 157, "dts": 4922100, "layer": 0, "discardable": false},
    {"frameId": 158, "dts": 4936544, "layer": 0, "discardable": false},
    {"frameId": 159, "dts": 4954111, "layer": 0, "discardable": false},
    {"frameId": 160, "dts": 4972233, "layer": 0, "discardable": false},
    {"frameId": 161, "dts": 4989077, "layer": 0, "discardable": false},
    {"frameId": 162, "dts": 5006477, "layer": 0, "discardable": false},
    {"frameId": 163, "dts": 5021688, "layer": 0, "discardable": false},
    {"frameId": 164, "dts": 5038244, "layer": 0, "discardable": false},
    {"frameId": 165, "dts": 5056655, "layer": 0, "discardable": false},
    {"frameId": 166, "dts": 5074266, "layer": 0, "discardable": false},
    {"frameId": 167, "dts": 5090500, "layer": 0, "discardable": false},
    {"frameId": 168, "dts": 5107355, "layer": 0, "discardable": false},
    {"frameId": 169, "dts": 5123988, "layer": 0, "discardable": false},
    {"frameId": 170, "dts": 5140533, "layer": 0, "discardable": false},
    {"frameId": 171, "dts": 5157344, "layer": 0, "discardable": false},
    {"frameId": 172, "dts": 5169366, "layer": 0, "discardable": false},
    {"frameId": 173, "dts": 5192400, "layer": 0, "discardable": false},
    {"frameId": 174, "dts": 5205333, "layer": 0, "discardable": false},
    {"frameId": 175, "dts": 5220455, "layer": 0, "discardable": false},
    {"frameId": 176, "dts": 5238155, "layer": 0, "discardable": false},
    {"frameId": 177, "dts": 5255511, "layer": 0, "discardable": false},
    {"frameId": 178, "dts": 5268855, "layer": 0, "discardable": false},
    {"frameId": 179, "dts": 5286033, "layer": 0, "discardable": false},
    {"frameId": 180, "dts": 5303288, "layer": 0, "discardable": false},
    {"frameId": 181, "dts": 5322344, "layer": 0, "discardable": false},
    {"frameId": 182, "dts": 5337500, "layer": 0, "discardable": false},
    {"frameId": 183, "dts": 5354477, "layer": 0, "discardable": false},
    {"frameId": 184, "dts": 5371688, "layer": 0, "discardable": false},
    {"frameId": 185, "dts": 5388277, "layer": 0, "discardable": false},
    {"frameId": 186, "dts": 5404144, "layer": 0, "discardable": false},
    {"frameId": 187, "dts": 5420900, "layer": 0, "discardable": false},
    {"frameId": 188, "dts": 5438633, "layer": 0, "discardable": false},
    {"frameId": 189, "dts": 5454622, "layer": 0, "discardable": false},
    {"frameId": 190, "dts": 5472411, "layer": 0, "discardable": false},
    {"frameId": 191, "dts": 5485600, "layer": 0, "discardable": false},
    {"frameId": 192, "dts": 5508800, "layer": 0, "discardable": false},
    {"frameId": 193, "dts": 5522444, "layer": 0, "discardable": false},
    {"frameId": 194, "dts": 5535800, "layer": 0, "discardable": false},
    {"frameId": 195, "dts": 5551988, "layer": 0, "discardable": false},
    {"frameId": 196, "dts": 5571388, "layer": 0, "discardable": false},
    {"frameId": 197, "dts": 5587600, "layer": 0, "discardable": false},
    {"frameId": 198, "dts": 5603055, "layer": 0, "discardable": false},
    {"frameId": 199, "dts": 5621622, "layer": 0, "discardable": false},
    {"frameId": 200, "dts": 5637611, "layer": 1, "discardable": false},
    {"frameId": 201, "dts": 5656044, "layer": 0, "discardable": false},
    {"frameId": 202, "dts": 5672411, "layer": 0, "discardable": false},
    {"frameId": 203, "dts": 5684966, "layer": 0, "discardable": false},
    {"frameId": 204, "dts": 5701388, "layer": 0, "discardable": false},
    {"frameId": 205, "dts": 5723211, "layer": 0, "discardable": false},
    {"frameId": 206, "dts": 5738744, "layer": 0, "discardable": false},
    {"frameId": 207, "dts": 5754733, "layer": 0, "discardable": false},
    {"frameId": 208, "dts": 5772611, "layer": 0, "discardable": false},
    {"frameId": 209, "dts": 5787066, "layer": 0, "discardable": false},
    {"frameId": 210, "dts": 5805022, "layer": 0, "discardable": false},
    {"frameId": 211, "dts": 5820377, "layer": 0, "discardable": false},
    {"frameId": 212, "dts": 5834111, "layer": 0, "discardable": false},
    {"frameId": 213, "dts": 5852644, "layer": 0, "discardable": false},
    {"frameId": 214, "dts": 5867477, "layer": 0, "discardable": false},
    {"frameId": 215, "dts": 5883522, "layer": 0, "discardable": false},
    {"frameId": 216, "dts": 5900488, "layer": 0, "discardable": false},
    {"frameId": 217, "dts": 5918922, "layer": 0, "discardable": false},
    {"frameId": 218, "dts": 5935500, "layer": 0, "discardable": false},
    {"frameId": 219, "dts": 5951088, "layer": 0, "discardable": false},
    {"frameId": 220, "dts": 5967911, "layer": 0, "discardable": false},
    {"frameId": 221, "dts": 5984800, "layer": 0, "discardable": false},
    {"frameId": 222, "dts": 6001855, "layer": 0, "discardable": false},
    {"frameId": 223, "dts": 6018911, "layer": 0, "discardable": false},
    {"frameId": 224, "dts": 6036788, "layer": 0, "discardable": false},
    {"frameId": 225, "dts": 6053677, "layer": 0, "discardable": false},
    {"frameId": 226, "dts": 6070366, "layer": 0, "discardable": false},
    {"frameId": 227, "dts": 6086844, "layer": 0, "discardable": false},
    {"frameId": 228, "dts": 6102155, "layer": 0, "discardable": false},
    {"frameId": 229, "dts": 6123355, "layer": 0, "discardable": false},
    {"frameId": 230, "dts": 6135188, "layer": 0, "discardable": false},
    {"frameId": 231, "dts": 6150944, "layer": 0, "discardable": false},
    {"frameId": 232, "dts": 6167322, "layer": 0, "discardable": false},
    {"frameId": 233, "dts": 6185144, "layer": 0, "discardable": false},
    {"frameId": 234, "dts": 6201688, "layer": 0, "discardable": false},
    {"frameId": 235, "dts": 6216544, "layer": 0, "discardable": false},
    {"frameId": 236, "dts": 6234688, "layer": 0, "discardable": false},
    {"frameId": 237, "dts": 6251133, "layer": 0, "discardable": false},
    {"frameId": 238, "dts": 6266866, "layer": 0, "discardable": false},
    {"frameId": 239, "dts": 6285088, "layer": 0, "discardable": false},
    {"frameId": 240, "dts": 6300955, "layer": 0, "discardable": false},
    {"frameId": 241, "dts": 6319644, "layer": 0, "discardable": false},
    {"frameId": 242, "dts": 6336655, "layer": 0, "discardable": false},
    {"frameId": 243, "dts": 6352433, "layer": 0, "discardable": false},
    {"frameId": 244, "dts": 6365988, "layer": 0, "discardable": false},
    {"frameId": 245, "dts": 6383388, "layer": 0, "discardable": false},
    {"frameId": 246, "dts": 6402766, "layer": 0, "discardable": false},
    {"frameId": 247, "dts": 6419133, "layer": 0, "discardable": false},
    {"frameId": 248, "dts": 6436155, "layer": 0, "discardable": false},
    {"frameId": 249, "dts": 6452755, "layer": 0, "discardable": false},
    {"frameId": 250, "dts": 6469388, "layer": 0, "discardable": false},
    {"frameId": 251, "dts": 6487722, "layer": 0, "discardable": false},
    {"frameId": 252, "dts": 6503300, "layer": 0, "discardable": false},
    {"frameId": 253, "dts": 6519811, "layer": 0, "discardable": false},
    {"frameId": 254, "dts": 6536544, "layer": 0, "discardable": false},
    {"frameId": 255, "dts": 6552244, "layer": 0, "discardable": false},
    {"frameId": 256, "dts": 6578100, "layer": 0, "discardable": false},
    {"frameId": 257, "dts": 6586144, "layer": 0, "discardable": false},
    {"frameId": 258, "dts": 6599833, "layer": 0, "discardable": false},
    {"frameId": 259, "dts": 6619566, "layer": 0, "discardable": false},
    {"frameId": 260, "dts": 6916466, "layer": 0, "discardable": false},
    {"frameId": 261, "dts": 6932355, "layer": 0, "discardable": false},
    {"frameId": 262, "dts": 6951011, "layer": 0, "discardable": false},
    {"frameId": 263, "dts": 6964911, "layer": 0, "discardable": false},
    {"frameId": 264, "dts": 6983766, "layer": 0, "discardable": false}
])"_json;

#endif //LAYER_INFO_RECORDING_H