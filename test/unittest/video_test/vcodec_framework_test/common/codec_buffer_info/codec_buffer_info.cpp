/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "codec_buffer_info.h"
#include "avbuffer_mock.h"
#include "common_mock.h"
#include "avformat_mock.h"

namespace OHOS {
namespace MediaAVCodec {
CodecBufferInfo::CodecBufferInfo(uint32_t index, Any handle, OH_AVCodecBufferAttr attr)
    : handle_(handle), index_(index), attr_(attr), isValid_(true)
{
}

CodecBufferHandle CodecBufferInfo::GetHandle() const
{
    return handle_;
}

CodecBufferType CodecBufferInfo::GetBufferType() const
{
    if (Any::IsSameTypeWith<OH_AVBuffer *>(handle_.any_)) {
        return TEST_AVBUFFER_CAPI;
    } else if (Any::IsSameTypeWith<OH_AVMemory *>(handle_.any_)) {
        return TEST_AVMEMORY_CAPI;
    } else if (Any::IsSameTypeWith<OH_AVFormat *>(handle_.any_)) {
        return TEST_AVFORMAT_CAPI;
    } else if (Any::IsSameTypeWith<std::shared_ptr<AVBufferMock>>(handle_.any_)) {
        return TEST_AVBUFFER_MOCK;
    } else if (Any::IsSameTypeWith<std::shared_ptr<AVMemoryMock>>(handle_.any_)) {
        return TEST_AVMEMORY_MOCK;
    } else if (Any::IsSameTypeWith<std::shared_ptr<FormatMock>>(handle_.any_)) {
        return TEST_AVFORMAT_MOCK;
    } else if (Any::IsSameTypeWith<ParameterWithAttrMock>(handle_.any_)) {
        return TEST_PARAMETER_WITH_ATTR_MOCK;
    } else {
        return TEST_INVALID_BUFFER_TYPE;
    }
}

uint32_t CodecBufferInfo::GetIndex() const
{
    return index_;
}

bool CodecBufferInfo::IsValid() const
{
    return isValid_;
}

OH_AVCodecBufferAttr CodecBufferInfo::GetAttr()
{
    switch (GetBufferType()) {
        case TEST_AVBUFFER_CAPI:
            OH_AVBuffer_GetBufferAttr(handle_, &attr_);
            break;
        case TEST_AVBUFFER_MOCK:
            std::shared_ptr<AVBufferMock>(handle_)->GetBufferAttr(attr_);
            break;
        default:
            break;
    }
    return attr_;
}

uint8_t *CodecBufferInfo::GetAddr()
{
    switch (GetBufferType()) {
        case TEST_AVBUFFER_CAPI:
            return OH_AVBuffer_GetAddr(handle_);
        case TEST_AVMEMORY_CAPI:
            return OH_AVMemory_GetAddr(handle_);
        case TEST_AVBUFFER_MOCK:
            return std::shared_ptr<AVBufferMock>(handle_)->GetAddr();
        case TEST_AVMEMORY_MOCK:
            return std::shared_ptr<AVMemoryMock>(handle_)->GetAddr();
        default:
            break;
    }
    return nullptr;
}

void CodecBufferInfo::SetAttr(OH_AVCodecBufferAttr attr)
{
    switch (GetBufferType()) {
        case TEST_AVBUFFER_CAPI:
            OH_AVBuffer_SetBufferAttr(handle_, &attr);
            break;
        case TEST_AVBUFFER_MOCK:
            std::shared_ptr<AVBufferMock>(handle_)->SetBufferAttr(attr);
            break;
        default:
            break;
    }
    attr_ = attr;
}

void CodecBufferInfo::Release()
{
    isValid_ = false;
}
} // namespace MediaAVCodec
} // namespace OHOS