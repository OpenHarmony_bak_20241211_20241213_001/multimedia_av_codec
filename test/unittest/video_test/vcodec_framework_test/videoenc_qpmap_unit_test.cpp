/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include "meta/meta_key.h"
#include "unittest_utils.h"
#include "codeclist_mock.h"
#include "venc_sample.h"
#include "native_avmagic.h"
#ifdef VIDEOENC_CAPI_UNIT_TEST
#include "native_avmagic.h"
#include "videoenc_capi_mock.h"
#define TEST_SUIT VideoEncQPMapCapiTest
#else
#define TEST_SUIT VideoEncQPMapInnerTest
#endif

using namespace std;
using namespace OHOS;
using namespace OHOS::MediaAVCodec;
using namespace testing::ext;
using namespace OHOS::MediaAVCodec::VCodecTestParam;
using namespace OHOS::Media;

namespace {
class TEST_SUIT : public testing::TestWithParam<int32_t> {
public:
    static void SetUpTestCase(void);
    static void TearDownTestCase(void);
    void SetUp(void);
    void TearDown(void);

    bool CreateVideoCodecByName(const std::string &decName);
    void CreateByNameWithParam(int32_t param);
    void SetFormatWithParam(int32_t param);
    void PrepareSource(int32_t param);
    bool GetQPMapCapability(int32_t param);

protected:
    std::shared_ptr<VideoEncSample> videoEnc_ = nullptr;
    std::shared_ptr<FormatMock> format_ = nullptr;
    std::shared_ptr<VEncCallbackTestExt> vencCallbackExt_ = nullptr;
    std::shared_ptr<VEncCallbackTest> vencCallback_ = nullptr;
    std::shared_ptr<VEncParamCallbackTest> vencParamCallback_ = nullptr;
};

void TEST_SUIT::SetUpTestCase(void)
{
    auto capability = CodecListMockFactory::GetCapabilityByCategory((CodecMimeType::VIDEO_AVC).data(), true,
                                                                    AVCodecCategory::AVCODEC_HARDWARE);
    ASSERT_NE(nullptr, capability) << (CodecMimeType::VIDEO_AVC).data() << " can not found!" << std::endl;
}

void TEST_SUIT::TearDownTestCase(void) {}

void TEST_SUIT::SetUp(void)
{
    std::shared_ptr<VEncSignal> vencSignal = std::make_shared<VEncSignal>();

    vencCallback_ = std::make_shared<VEncCallbackTest>(vencSignal);
    ASSERT_NE(nullptr, vencCallback_);

    vencCallbackExt_ = std::make_shared<VEncCallbackTestExt>(vencSignal);
    ASSERT_NE(nullptr, vencCallbackExt_);

    vencParamCallback_ = std::make_shared<VEncParamCallbackTest>(vencSignal);
    ASSERT_NE(nullptr, vencParamCallback_);

    videoEnc_ = std::make_shared<VideoEncSample>(vencSignal);
    ASSERT_NE(nullptr, videoEnc_);

    format_ = FormatMockFactory::CreateFormat();
    ASSERT_NE(nullptr, format_);
}

void TEST_SUIT::TearDown(void)
{
    if (format_ != nullptr) {
        format_->Destroy();
    }
    videoEnc_ = nullptr;
}

bool TEST_SUIT::CreateVideoCodecByName(const std::string &name)
{
    if (!videoEnc_->CreateVideoEncMockByName(name)) {
        return false;
    }
    int32_t ret = videoEnc_->isAVBufferMode_ ? videoEnc_->SetCallback(vencCallbackExt_)
                                             : videoEnc_->SetCallback(vencCallback_);
    return ret == AV_ERR_OK;
}

void TEST_SUIT::CreateByNameWithParam(int32_t param)
{
    std::string codecName = "";
    std::shared_ptr<AVCodecList> codecCapability = AVCodecListFactory::CreateAVCodecList();
    CapabilityData *capabilityData = nullptr;
    switch (param) {
        case VCodecTestCode::HW_AVC:
            capabilityData = codecCapability->GetCapability(CodecMimeType::VIDEO_AVC.data(), true,
                                                            AVCodecCategory::AVCODEC_HARDWARE);
            break;
        case VCodecTestCode::HW_HEVC:
            capabilityData = codecCapability->GetCapability(CodecMimeType::VIDEO_HEVC.data(), true,
                                                            AVCodecCategory::AVCODEC_HARDWARE);
            break;
        default:
            capabilityData = codecCapability->GetCapability(CodecMimeType::VIDEO_AVC.data(), true,
                                                            AVCodecCategory::AVCODEC_SOFTWARE);
            break;
    }
    ASSERT_NE(capabilityData, nullptr);
    codecName = capabilityData->codecName;
    std::cout << "CodecName: " << codecName << "\n";
    ASSERT_TRUE(CreateVideoCodecByName(codecName));
}

void TEST_SUIT::PrepareSource(int32_t param)
{
    const ::testing::TestInfo *testInfo_ = ::testing::UnitTest::GetInstance()->current_test_info();
    string prefix = "/data/test/media/";
    string fileName = testInfo_->name();
    auto check = [](char it) { return it == '/'; };
    (void)fileName.erase(std::remove_if(fileName.begin(), fileName.end(), check), fileName.end());
    videoEnc_->SetOutPath(prefix + fileName);
}

void TEST_SUIT::SetFormatWithParam(int32_t param)
{
    (void)param;
    format_->PutIntValue(MediaDescriptionKey::MD_KEY_WIDTH, DEFAULT_WIDTH_VENC);
    format_->PutIntValue(MediaDescriptionKey::MD_KEY_HEIGHT, DEFAULT_HEIGHT_VENC);
    format_->PutIntValue(MediaDescriptionKey::MD_KEY_PIXEL_FORMAT, AV_PIXEL_FORMAT_NV12);
}

bool TEST_SUIT::GetQPMapCapability(int32_t param)
{
    std::string codecName = "";
    std::shared_ptr<AVCodecList> codecCapability = AVCodecListFactory::CreateAVCodecList();
    CapabilityData *capabilityData = nullptr;
    switch (param) {
        case VCodecTestCode::HW_AVC:
            capabilityData = codecCapability->GetCapability(CodecMimeType::VIDEO_AVC.data(), true,
                                                            AVCodecCategory::AVCODEC_HARDWARE);
            break;
        case VCodecTestCode::HW_HEVC:
            capabilityData = codecCapability->GetCapability(CodecMimeType::VIDEO_HEVC.data(), true,
                                                            AVCodecCategory::AVCODEC_HARDWARE);
            break;
        default:
            capabilityData = codecCapability->GetCapability(CodecMimeType::VIDEO_AVC.data(), true,
                                                            AVCodecCategory::AVCODEC_SOFTWARE);
            break;
    }
    if (capabilityData == nullptr) {
        std::cout << "capabilityData is nullptr" << std::endl;
        return false;
    }
    if (capabilityData->featuresMap.count(
        static_cast<int32_t>(AVCapabilityFeature::VIDEO_ENCODER_QP_MAP))) {
        std::cout << "Support QP Map" << std::endl;
        return true;
    }
    std::cout << "Not support QP Map" << std::endl;
    return false;
}

INSTANTIATE_TEST_SUITE_P(, TEST_SUIT, testing::Values(HW_AVC, HW_HEVC));

/**
 * @tc.name: VideoEncoder_QPMapCapability_001
 * @tc.desc: configure to enable QPMap Capability for video encode, buffer mode
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_001, TestSize.Level1)
{
    if (!GetQPMapCapability(GetParam())) {
        return;
    }
    videoEnc_->isAVBufferMode_ = true;
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 1);
    ASSERT_EQ(AV_ERR_OK, videoEnc_->Configure(format_));
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Start());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Stop());
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_002
 * @tc.desc: configure to enable QPMap Capability for video encode, surface mode（AVMemory） with set parametercallback
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_002, TestSize.Level1)
{
    if (!GetQPMapCapability(GetParam())) {
        return;
    }
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    ASSERT_EQ(AV_ERR_OK, videoEnc_->SetCallback(vencParamCallback_));
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 1);
    ASSERT_EQ(AV_ERR_OK, videoEnc_->Configure(format_));
    ASSERT_EQ(AV_ERR_OK, videoEnc_->CreateInputSurface());
#ifdef VIDEOENC_CAPI_UNIT_TEST
    ASSERT_EQ(AV_ERR_UNSUPPORT, videoEnc_->Start());
#else
    ASSERT_EQ(AVCS_ERR_UNSUPPORT, videoEnc_->Start());
#endif
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_003
 * @tc.desc: configure to enable QPMap Capability for video encode, surface mode（AVBuffer） with set parametercallback
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_003, TestSize.Level1)
{
    if (!GetQPMapCapability(GetParam())) {
        return;
    }
    videoEnc_->isAVBufferMode_ = true;
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    ASSERT_EQ(AV_ERR_OK, videoEnc_->SetCallback(vencParamCallback_));
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 1);
    ASSERT_EQ(AV_ERR_OK, videoEnc_->Configure(format_));
    ASSERT_EQ(AV_ERR_OK, videoEnc_->CreateInputSurface());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Start());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Stop());
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_004
 * @tc.desc: enable qp and set QP Map per frame for video encode, buffer mode
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_004, TestSize.Level1)
{
    if (!GetQPMapCapability(GetParam())) {
        return;
    }
    videoEnc_->isAVBufferMode_ = true;
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    videoEnc_->enableQPMapCapability = true;
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 1);
    ASSERT_EQ(AV_ERR_OK, videoEnc_->Configure(format_));
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Start());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Stop());
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_005
 * @tc.desc: enable qp and set QP Map per frame for video encode, surface mode（AVMemory）
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_005, TestSize.Level1)
{
    if (!GetQPMapCapability(GetParam())) {
        return;
    }
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    ASSERT_EQ(AV_ERR_OK, videoEnc_->SetCallback(vencParamCallback_));
    videoEnc_->enableQPMapCapability = true;
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 1);
    ASSERT_EQ(AV_ERR_OK, videoEnc_->Configure(format_));
    ASSERT_EQ(AV_ERR_OK, videoEnc_->CreateInputSurface());
#ifdef VIDEOENC_CAPI_UNIT_TEST
    ASSERT_EQ(AV_ERR_UNSUPPORT, videoEnc_->Start());
#else
    ASSERT_EQ(AVCS_ERR_UNSUPPORT, videoEnc_->Start());
#endif
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_006
 * @tc.desc: enable qp and set QP Map per frame for video encode, surface mode（AVBuffer）
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_006, TestSize.Level1)
{
    if (!GetQPMapCapability(GetParam())) {
        return;
    }
    videoEnc_->isAVBufferMode_ = true;
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    ASSERT_EQ(AV_ERR_OK, videoEnc_->SetCallback(vencParamCallback_));
    videoEnc_->enableQPMapCapability = true;
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 1);
    ASSERT_EQ(AV_ERR_OK, videoEnc_->Configure(format_));
    ASSERT_EQ(AV_ERR_OK, videoEnc_->CreateInputSurface());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Start());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Stop());
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_007
 * @tc.desc: disable qp and set QP Map per frame for video encode, buffer mode
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_007, TestSize.Level1)
{
    if (!GetQPMapCapability(GetParam())) {
        return;
    }
    videoEnc_->isAVBufferMode_ = true;
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    videoEnc_->enableQPMapCapability = true;
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 0);
    ASSERT_EQ(AV_ERR_OK, videoEnc_->Configure(format_));
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Start());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Stop());
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_008
 * @tc.desc: disable qp and set QP Map per frame for video encode, surface mode（AVMemory）
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_008, TestSize.Level1)
{
    if (!GetQPMapCapability(GetParam())) {
        return;
    }
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    ASSERT_EQ(AV_ERR_OK, videoEnc_->SetCallback(vencParamCallback_));
    videoEnc_->enableQPMapCapability = true;
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 0);
    ASSERT_EQ(AV_ERR_OK, videoEnc_->Configure(format_));
    ASSERT_EQ(AV_ERR_OK, videoEnc_->CreateInputSurface());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Start());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Stop());
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_009
 * @tc.desc: disable qp and set QP Map per frame for video encode, surface mode（AVBuffer）
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_009, TestSize.Level1)
{
    if (!GetQPMapCapability(GetParam())) {
        return;
    }
    videoEnc_->isAVBufferMode_ = true;
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    ASSERT_EQ(AV_ERR_OK, videoEnc_->SetCallback(vencParamCallback_));
    videoEnc_->enableQPMapCapability = true;
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 0);
    ASSERT_EQ(AV_ERR_OK, videoEnc_->Configure(format_));
    ASSERT_EQ(AV_ERR_OK, videoEnc_->CreateInputSurface());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Start());
    EXPECT_EQ(AV_ERR_OK, videoEnc_->Stop());
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_010
 * @tc.desc: unsupport platform, configure to enable QPMap Capability for video encode（AVBuffer)
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_010, TestSize.Level1)
{
    if (GetQPMapCapability(GetParam())) {
        return;
    }
    videoEnc_->isAVBufferMode_ = true;
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 1);
#ifdef VIDEOENC_CAPI_UNIT_TEST
    ASSERT_EQ(AV_ERR_UNSUPPORT, videoEnc_->Configure(format_));
#else
    ASSERT_EQ(AVCS_ERR_UNSUPPORT, videoEnc_->Configure(format_));
#endif
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_011
 * @tc.desc: unsupport platform, configure to enable QPMap Capability for video encode（AVMemory)
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_011, TestSize.Level1)
{
    if (GetQPMapCapability(GetParam())) {
        return;
    }
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    ASSERT_EQ(AV_ERR_OK, videoEnc_->SetCallback(vencParamCallback_));
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 1);
#ifdef VIDEOENC_CAPI_UNIT_TEST
    ASSERT_EQ(AV_ERR_UNSUPPORT, videoEnc_->Configure(format_));
#else
    ASSERT_EQ(AVCS_ERR_UNSUPPORT, videoEnc_->Configure(format_));
#endif
}

/**
 * @tc.name: VideoEncoder_QPMapCapability_012
 * @tc.desc: unsupport platform, configure to enable QPMap Capability for video encode, surface mode（AVBuffer）
 * @tc.type: FUNC
 */
HWTEST_P(TEST_SUIT, VideoEncoder_QPMapCapability_012, TestSize.Level1)
{
    if (GetQPMapCapability(GetParam())) {
        return;
    }
    videoEnc_->isAVBufferMode_ = true;
    CreateByNameWithParam(GetParam());
    SetFormatWithParam(GetParam());
    PrepareSource(GetParam());
    ASSERT_EQ(AV_ERR_OK, videoEnc_->SetCallback(vencParamCallback_));
    format_->PutIntValue(Media::Tag::VIDEO_ENCODER_ENABLE_QP_MAP, 1);
#ifdef VIDEOENC_CAPI_UNIT_TEST
    ASSERT_EQ(AV_ERR_UNSUPPORT, videoEnc_->Configure(format_));
#else
    ASSERT_EQ(AVCS_ERR_UNSUPPORT, videoEnc_->Configure(format_));
#endif
}
} // namespace

int main(int argc, char **argv)
{
    testing::GTEST_FLAG(output) = "xml:./";
    for (int i = 0; i < argc; ++i) {
        cout << argv[i] << endl;
        if (strcmp(argv[i], "--need_dump") == 0) {
            VideoEncSample::needDump_ = true;
            DecArgv(i, argc, argv);
        }
    }
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}